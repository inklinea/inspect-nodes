#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Inspect Nodes - Label nodes with coordinates / output to msgbox
# Appears Under Extensions>Info>Inspect Nodes
# An Inkscape 1.2+ extension
##############################################################################


from inkex import (Boolean, Circle, EffectExtension, Layer, PathElement,
                   TextElement, errormsg)
from inkex.units import convert_unit


class InspectNodes(EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--inspect_nodes_notebook", type=str, dest="inspect_nodes_notebook", default=0)

        pars.add_argument("--points_cb", type=Boolean, dest="points_cb", default=True)
        pars.add_argument("--coords_cb", type=Boolean, dest="coords_cb", default=True)
        pars.add_argument("--connector_cb", type=Boolean, dest="connector_cb", default=True)
        pars.add_argument("--msgbox_cb", type=Boolean, dest="msgbox_cb", default=True)

        pars.add_argument("--font_size_float", type=float, dest="font_size_float", default=16)

        pars.add_argument("--prefix_string", type=str, dest="prefix_string", default='')
        pars.add_argument("--suffix_string", type=str, dest="suffix_string", default='')

        pars.add_argument("--font_units_combo", type=str, dest="font_units_combo", default='user')

        pars.add_argument("--display_units_combo", type=str, dest="display_units_combo", default='user')

        pars.add_argument("--point_decimals", type=int, dest="point_decimals", default=2)
        pars.add_argument("--point_units", type=Boolean, dest="point_units", default=True)

    def effect(self):

        selection_list = self.svg.selected
        selected_nodes = self.options.selected_nodes

        if len(selection_list) < 1 or len(selected_nodes) < 1:
            errormsg('No nodes selected')
            return

        self.inspect_layer = Layer()
        if self.options.points_cb is True or self.options.coords_cb is True:
            self.inspect_layer.set('id', self.svg.get_unique_id('Inspection Layer_'))
            self.svg.append(self.inspect_layer)

        for element in selection_list:
            path_d_abs = element.path.to_absolute().transform(element.composed_transform())
            csp = path_d_abs.to_superpath()

            for item in selected_nodes:
                path_id, subpath_index, node_index = item.split(':')
                if element.get('id') == path_id:
                    x, y = csp[int(subpath_index)][int(node_index)][1]
                    self.process_node(x, y, convert_unit(1, self.svg.unit))

        if len(self.inspect_layer.getchildren()) < 1:
            self.inspect_layer.delete()

    def process_node(self, x, y, r):
        chosen_units = self.options.display_units_combo
        if chosen_units == 'user':
            chosen_units = self.svg.unit

        if self.options.points_cb is True:
            point_circle = self.point_circles(x, y, r)
            self.inspect_layer.append(point_circle)

        if self.options.coords_cb is True:
            point_label = self.point_labels(x, y, chosen_units)
            self.inspect_layer.append(point_label)

        if self.options.points_cb is True and self.options.coords_cb is True and self.options.connector_cb is True:
            self.make_connection(point_circle, point_label)

        if self.options.msgbox_cb is True:
            x = convert_unit(x, chosen_units, self.svg.unit)
            y = convert_unit(y, chosen_units, self.svg.unit)
            errormsg(f'[{x:.{self.options.point_decimals}f}, {y:.{self.options.point_decimals}f}] {chosen_units}')

    def point_circles(self, x, y, r):
        point_circle = Circle()
        point_circle.set('r', r)
        point_circle.set('cx', x)
        point_circle.set('cy', y)
        point_circle.style['stroke'] = 'red'

        return point_circle

    def point_labels(self, x, y, chosen_units):
        x_text = convert_unit(x, chosen_units, self.svg.unit)
        x_text = f'{x_text:.{self.options.point_decimals}f}'

        y_text = convert_unit(y, chosen_units, self.svg.unit)
        y_text = f'{y_text:.{self.options.point_decimals}f}'

        if self.options.point_units:
            units = f' {chosen_units}'
        else:
            units = ''

        point_label = TextElement()
        point_label.text = f'{self.options.prefix_string}{x_text}{units}, {y_text}{units}{self.options.suffix_string}'
        point_label.set('x', x)
        point_label.set('y', y)
        point_label.style['font-size'] = f'{self.options.font_size_float}{self.options.font_units_combo}'

        return point_label

    def make_connection(self, start_object, end_object, connector_type='polyline', connector_curvature=0):

        line_style = {'stroke': 'black',
                      'stroke-width': '0.5',
                      'fill': 'none',
                      'stroke-dasharray': '1,1'
                      }

        my_connector = PathElement()
        my_connector.style = line_style
        my_connector.set('inkscape:connector-type', connector_type)
        my_connector.set('inkscape:connector-curvature', str(connector_curvature))
        my_connector.set('inkscape:connection-start', f'#{start_object.get_id()}')
        my_connector.set('inkscape:connection-end', f'#{end_object.get_id()}')
        self.inspect_layer.append(my_connector)


if __name__ == '__main__':
    InspectNodes().run()
