# Inspect Nodes

# An Inkscape 1.2+ extension

▶ Add points or coordinate labels to path nodes

▶ Points can be connected to labels

▶ Only works on paths

▶ Appears Under Extensions>Info>Inspect Nodes

▶ A shortcut can be made to
  inklinea.inspect_nodes.noprefs
